<?php
/**
 * @file
 * Administration pages for included tax
 */

/**
 * Included tax administration page
 */
function uc_included_tax_admin($form_state, $id = 0) {
  $form = array();
  $form['taxes'] = array(
    '#tree' => TRUE,
  );

  if ($id) {
    $label = 'Edit tax';
    $res = db_query("SELECT * FROM {uc_included_tax} WHERE tax_id = %d", $id);
    $edit  = db_fetch_object($res);
  }
  else {
    $label = 'Add a new tax';
    $edit->tax_id = NULL;
    $edit->name = NULL;
    $edit->pcid = array();
    $edit->rate = 0.00;
  }

  $result = db_query("SELECT * FROM {uc_included_tax}");

  while ($r = db_fetch_object($result)) {
    $tax_id = $r->tax_id;
    $form['taxes'][$tax_id]['delete'] = array(
      '#type' => 'checkbox',
      '#default_value' => 0,
    );
    if ($id && $id == tax_id) {
      $edit = $r;
    }

  }
  
  // Get product classes
  $product_classes = array();
  $res = db_query('SELECT pcid, name FROM {uc_product_classes}');
  while ($r = db_fetch_object($res)) {
     $product_classes[$r->pcid] = $r->name;
   }

  $form['tax'] = array(
    '#type' => 'fieldset',
    '#title' => t($label),
    '#description' => t('Included tax rate configuration.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['tax']['tax_id'] = array(
    '#type' => 'hidden',
    '#value' => $edit->tax_id,
  );
  
  $form['tax']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Tax name'),
    '#default_value' => '',
    '#default_value' => $edit->name,
    '#size' => 40,
    '#description' => t('Displayed tax name.'),
  );

  $form['tax']['rate'] = array(
    '#type' => 'textfield',
    '#title' => t('Tax Rate'),
    '#default_value' => number_format($edit->rate, variable_get('uc_currency_prec', 2), '.', ''),
    '#size' => 12,
    '#description' => t('Tax rate, example: use 0.10 for 10%.'),
  );
  
  $form['tax']['pcid'] = array(
    '#type' => 'select',
    '#title' => t('Product classes'),
    '#options' => $product_classes,
    '#default_value' => count($edit->pcid)? unserialize($edit->pcid): array(),
    '#multiple' => TRUE,
    '#description' => t('Select product types to apply this tax.'),
  );
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Changes'),
  );
  return $form;
}

/**
 * Admin form validation
 */
function uc_included_tax_admin_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Check duplicated product classes
  
  $q = 'SELECT pcid FROM {uc_included_tax}';
  if ($values['tax_id']) {
   $q .= sprintf(' WHERE tax_id != %d', $values['tax_id']);
  }
  $res = db_query($q);
  $used = array();
  while ($row = db_fetch_object($res)) {
     $pcids = unserialize($row->pcid);
     foreach($values['pcid'] as $pcid){
        if (array_search($pcid, $pcids)) {
           $used[] = $pcid;
        }
     } 
  }
   if (count($used)){
      form_set_error('pcid', t('Product class defined in another tax: %list', array('%list' => implode(', ', $used))));
   }

  if (!is_numeric($values['rate'])) {
    form_set_error('rate', t('The tax rate must be a number.'));
  }
}

/**
 * Admin form submit
 */
function uc_included_tax_admin_submit($form, &$form_state) {
  $values = $form_state['values'];
  // check for and handle any deletions
  $todelete = array();
  if (is_array($values['taxes'])) {
    foreach ($values['taxes'] as $tax_id => $value) {
      if ($value['delete']) {
        $todelete[] = $tax_id;
      }
    if (count($todelete)) {
       db_query("DELETE FROM {uc_included_tax} WHERE tax_id IN ('%s')", implode(', ', $todelete));
       drupal_set_message(t('Deleted taxes.'));
       return;
    }
    }
  }
  if ($values['tax_id']){
     db_query("UPDATE {uc_included_tax} SET name='%s', rate=%f, pcid='%s' WHERE tax_id = %d", 
               $values['name'], $values['rate'], serialize($values['pcid']), $values['tax_id']);
  }
  else {
     db_query("INSERT INTO {uc_included_tax} (name, rate, pcid) VALUES ('%s', %f, '%s')", 
               $values['name'], $values['rate'], serialize($values['pcid']));
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Theme admin form
 */
function theme_uc_included_tax_admin($form) {
  $header = array(t('Tax Name'), t('Rate'), t('Product classes'), t('Delete'));
  $result = db_query("SELECT tax_id, name, rate, pcid FROM {uc_included_tax}");
  
  while ($r = db_fetch_object($result)) {
    $row = array();
    $row[] = ($r->name);
    $row[] = number_format($r->rate, 2, variable_get('uc_currency_dec', '.'), '');
    $row[] = implode(', ', unserialize($r->pcid));
    $row[] = drupal_render($form['taxes'][$r->tax_id]['delete']);
    $row[] = l('edit', 'admin/store/settings/included_tax/' . $r->tax_id);
    $rows[] = $row;
  }

  if (count($rows) == 0) {
    $rows[] = array(array(
        'data' => t('No taxes found.'),
        'colspan' => 4,
      ));
  }
  $output .= theme('table', $header, $rows);

  $output .= drupal_render($form);

  return $output;
}
